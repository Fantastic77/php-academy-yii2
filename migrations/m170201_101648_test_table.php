<?php

use yii\db\Migration;

class m170201_101648_test_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'content' => $this->text(),
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('news');
    }
}
